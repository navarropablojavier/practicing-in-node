const { ClubsController } = include("controllers");

module.exports = (router) => {
  router.route("/").get(ClubsController.fetch).post(ClubsController.create);

  router
    .route("/:id")
    .get(ClubsController.fetchOne)
    .put(ClubsController.save)
    .delete(ClubsController.delete);

  return router;
};
