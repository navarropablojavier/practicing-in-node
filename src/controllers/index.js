module.exports = {
  ClubsController: require("./clubs"),
  CountryController: require("./country"),
  ProvincesController: require("./provinces"),
  CarsController: require("./cars"),
  PersonsController: require("./persons"),
  StatusController: require("./status"),
};
