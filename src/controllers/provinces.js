const { Provinces } = include("models");
class ProvincesController {
  static async fetch(req, res, next) {
    try {
      const provinces = await Provinces.find({
        ...req.query,
        deleted: null,
      });
      res.send(provinces);
    } catch (error) {
      next(error);
    }
  }

  static async fetchOne(req, res, next) {
    try {
      const provinces = await Provinces.findOne(req.params);
      res.send(provinces);
    } catch (error) {
      next(error);
    }
  }

  static async create(req, res, next) {
    try {
      const result = await Provinces.insertOne(req.body);
      res.send({
        success: true,
        result,
      });
    } catch (error) {
      next(error);
    }
  }

  static async save(req, res, next) {
    try {
      const result = await Provinces.updatedOne(req.params, req.body);
      res.send({
        success: true,
        result,
      });
    } catch (error) {
      next(error);
    }
  }

  static async delete(req, res, next) {
    try {
      const result = await Provinces.deletedOne(req.params.id);
      res.send({
        success: true,
        result,
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = ProvincesController;
