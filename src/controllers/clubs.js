const { Clubs } = include("models");
class ClubsController {
  static async fetch(req, res, next) {
    try {
      const clubs = await Clubs.find({
        ...req.query,
        deleted: null,
      });
      res.send(clubs);
    } catch (error) {
      next(error);
    }
  }

  static async fetchOne(req, res, next) {
    try {
      const clubs = await Clubs.findOne(req.params);
      res.send(clubs);
    } catch (error) {
      next(error);
    }
  }

  static async create(req, res, next) {
    try {
      const result = await Clubs.insertOne(req.body);
      res.send({
        success: true,
        result,
      });
    } catch (error) {
      next(error);
    }
  }

  static async save(req, res, next) {
    try {
      const result = await Clubs.updateOne(req.params, req.body);
      res.send({
        success: true,
        result,
      });
    } catch (error) {
      next(error);
    }
  }

  static async delete(req, res, next) {
    try {
      const result = await Clubs.deletedOne(req.params.id);
      res.send({
        success: true,
        result,
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = ClubsController;
