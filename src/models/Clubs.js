const ModelCreate = include("helpers/modelCreate");

const tableName = "clubs";
const name = "Clubs";
const selectableProps = ["id", "name", "iso2", "code", "deleted"];

class Clubs extends ModelCreate {
  constructor(props) {
    super({
      ...props,
      tableName,
      name,
      selectableProps,
    });
  }
}
module.exports = (knex) => new Clubs({ knex });
