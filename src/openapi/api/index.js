const clubs = require("./clubs");
const country = require("./country");
const provinces = require("./provinces");
const cars = require("./cars");
const persons = require("./persons");

module.exports = { ...clubs, ...country, ...cars, ...provinces, ...persons };
