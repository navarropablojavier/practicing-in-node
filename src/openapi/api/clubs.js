module.exports = {
  "/api/clubs": {
    get: {
      security: [],
      summary: "List Clubs",
      parameters: [
        {
          in: "query",
          name: "code",
          schema: {
            type: "string",
            pattern: "^[A-Z]{2}$",
          },
          description: "Código del club solicitado",
        },
      ],
      responses: {
        200: {
          description: "list of Clubs",
          content: {
            "application/json": {
              schema: {
                type: "array",
                items: { $ref: "#/components/schemas/Clubs" },
              },
            },
          },
        },
        default: {
          description: "Error",
          content: {
            "application/json": {
              schema: { $ref: "#/components/schemas/Error" },
            },
          },
        },
      },
    },
    post: {
      security: [],
      requestBody: {
        description: "Optional description in *Markdown*",
        required: true,
        content: {
          "application/json": {
            schema: { $ref: "#/components/schemas/Clubs" },
          },
        },
      },
      responses: {
        200: {
          description: "list of Clubs",
          content: {
            "application/json": {
              schema: {
                type: "object",
                properties: {},
              },
            },
          },
        },
        default: {
          description: "Error",
          content: {
            "application/json": {
              schema: { $ref: "#/components/schemas/Error" },
            },
          },
        },
      },
    },
  },
  "/api/clubs/{id}": {
    get: {
      security: [],
      parameters: [
        {
          in: "path",
          name: "id",
          schema: {
            type: "string",
            format: "uuid",
          },
          required: true,
          description: "Id del club solicitado",
        },
      ],
      responses: {
        200: {
          description: "list of Clubs",
          content: {
            "application/json": {
              schema: { $ref: "#/components/schemas/Clubs" },
            },
          },
        },
        default: {
          description: "Error",
          content: {
            "application/json": {
              schema: { $ref: "#/components/schemas/Error" },
            },
          },
        },
      },
    },
    put: {
      security: [],
      parameters: [
        {
          in: "path",
          name: "id",
          schema: {
            type: "string",
            format: "uuid",
          },
          required: true,
          description: "Id del club solicitado",
        },
      ],
      requestBody: {
        description: "Optional description in *Markdown*",
        required: true,
        content: {
          "application/json": {
            schema: { $ref: "#/components/schemas/Clubs" },
          },
        },
      },
      responses: {
        200: {
          description: "list of Clubs",
          content: {
            "application/json": {
              schema: {
                type: "object",
                properties: {},
              },
            },
          },
        },
        default: {
          description: "Error",
          content: {
            "application/json": {
              schema: { $ref: "#/components/schemas/Error" },
            },
          },
        },
      },
    },
    delete: {
      security: [],
      parameters: [
        {
          in: "path",
          name: "id",
          schema: {
            type: "string",
            format: "uuid",
          },
          required: true,
          description: "Id del club solicitado",
        },
      ],
      responses: {
        200: {
          description: "list of Clubs",
          content: {
            "application/json": {
              schema: {
                type: "object",
                properties: {},
              },
            },
          },
        },
        default: {
          description: "Error",
          content: {
            "application/json": {
              schema: { $ref: "#/components/schemas/Error" },
            },
          },
        },
      },
    },
  },
};
