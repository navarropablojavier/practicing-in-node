require("../../src/global");

const { Clubs } = include("/models");
const clubs = require("./clubs.json");
exports.seed = async (knex) => {
  await knex(Clubs.tableName).del();
  try {
    await Clubs.startTransaction();
    await Promise.all(clubs.map((clubs) => Clubs.insertOne(clubs)));
    return Clubs.commitTransaction();
  } catch (err) {
    console.log("err: ", err);
  }
};
